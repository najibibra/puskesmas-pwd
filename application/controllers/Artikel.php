<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {

	public function index(){
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$menu = 'informasi';
		$title = 'Artikel Kesehatan';
		$dataKontak = $this->Kontak_Model->Get('tb_kontak');
		$data = array(
			'dataArtikel' => $dataArtikel,
			'menu' 	   => $menu,
			'title'      => $title,
			'dataKontak' => $dataKontak,
		);
		$this->load->view('artikel', $data);
	}

	public function detail($id){
		$menu = 'informasi';
		$title = 'Artikel Kesehatan';
		$artikel=($this->Artikel_Model->ambil_data_id($id));
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$dataKontak = $this->Kontak_Model->Get('tb_kontak');
		if (!empty($artikel)) {
			$data = array(
				'id_artikel'   => set_value('id_artikel',$artikel->id_artikel),
				'judul' => set_value('judul',$artikel->judul),
				'isi'       => set_value('isi',$artikel->isi),
				'tanggal'   => set_value('tanggal',$artikel->tanggal),
				'gambar'   => set_value('gambar',$artikel->gambar),
				'menu' 	   => $menu,
				'title'      => $title,
				'dataArtikel' => $dataArtikel,
				'dataKontak' => $dataKontak,
			);
			$this->load->view('artikel_detail', $data);
		} else {
			$this->load->view('not-found');
		}
	}

}
