<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {
	
	public function index(){
		$menu = 'kontak';
		$title = 'Hubungi Kami';
		$dataKontak = $this->Kontak_Model->Get('tb_kontak');
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'dataKontak' => $dataKontak,
			'dataArtikel' => $dataArtikel,
		);
		$this->load->view('kontak', $data);
	}

}
