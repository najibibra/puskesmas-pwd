<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persalinan extends CI_Controller {
	
	public function index(){
		$menu = 'informasi';
		$title = 'Pelayanan Persalinan';
		$database = $this->Persalinan_Model->Get('tb_persalinan');
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$dataKontak = $this->Kontak_Model->Get('tb_kontak');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'data' => $database,
			'dataArtikel' => $dataArtikel,
			'dataKontak' => $dataKontak,
		);
		$this->load->view('persalinan', $data);
	}

}
