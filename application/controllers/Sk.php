<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sk extends CI_Controller {
	
	public function index(){
		$menu = 'profil';
		$title = 'Surat Keputusan';
		$data = $this->Sk_Model->Get('tb_sk');
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$dataKontak = $this->Kontak_Model->Get('tb_kontak');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'data'      => $data,
			'dataArtikel' => $dataArtikel,
			'dataKontak' => $dataKontak,
		);
		$this->load->view('sk', $data);
	}

}
