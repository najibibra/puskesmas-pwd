<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sop extends CI_Controller {
	
	public function index(){
		$menu = 'profil';
		$title = 'Standar Operasional Prosedur';
		$data = $this->Sop_Model->Get('tb_sop');
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$dataKontak = $this->Kontak_Model->Get('tb_kontak');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'data'      => $data,
			'dataArtikel' => $dataArtikel,
			'dataKontak' => $dataKontak,
		);
		$this->load->view('sop', $data);
	}

}
