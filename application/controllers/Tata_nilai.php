<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tata_nilai extends CI_Controller {
	
	public function index(){
		$menu = 'profil';
		$title = 'Tata Nilai';
		$data = $this->Profil_Model->Get('tb_profil');
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$dataKontak = $this->Kontak_Model->Get('tb_kontak');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'data'      => $data,
			'dataArtikel' => $dataArtikel,
			'dataKontak' => $dataKontak,
		);
		$this->load->view('tata_nilai', $data);
	}

}
