<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	
	public function index(){
		$menu = 'home';
		$dataBackground = $this->Background_Model->Get('tb_background');
		$dataJadwal = $this->Jadwal_Model->Get('tb_jadwal_pelayanan');
		$dataArtikel = $this->Artikel_Model->Get3_latest('tb_artikel');
		$dataKontak = $this->Kontak_Model->Get('tb_kontak');
		$data = array(
			'menu' 	   => $menu,
			'dataBackground' => $dataBackground,
			'dataJadwal' => $dataJadwal,
			'dataArtikel' => $dataArtikel,
			'dataKontak' => $dataKontak,
		);
		$this->load->view('home', $data);
	}

}
