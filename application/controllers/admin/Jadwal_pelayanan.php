<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_pelayanan extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Jadwal_Model->Get('tb_jadwal_pelayanan');
		$data = array('data' => $data);
		$this->load->view('admin/jadwal_pelayanan', $data);
	}

	public function edit($id){
		$jadwal_pelayanan=($this->Jadwal_Model->ambil_data_id($id));
		$data = array(
            'id'   => set_value('id',$jadwal_pelayanan->id),
            'jadwal_pelayanan'   => set_value('jadwal_pelayanan',$jadwal_pelayanan->jadwal_pelayanan),
            'action' 	  => site_url('admin/jadwal_pelayanan/updateData')
		);
		$this->load->view('admin/jadwal_pelayanan_edit', $data);
	}

	public function updateData(){
		date_default_timezone_set('Asia/Jakarta');
		$id = $this->input->post('id');
		$tanggal = date('Y:m:d H:i:s');
		$path = './assets/general/';
		$case = array('id' => $id);

		$config['upload_path'] = './assets/general';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = 1024 * 5; // <= 5Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->upload->initialize($config);

		if(!empty($_FILES['file']['name'])){
			if($this->upload->do_upload('file')){
				$file = $this->upload->data();
				$data = array(
					'tanggal' => $tanggal,
					'jadwal_pelayanan' => $file['file_name'],
				);

				@unlink($path.$this->input->post('file-lama'));
				$this->Jadwal_Model->updateJadwal($data, $case);
				echo "<script>
				alert('Jadwal Pelayanan Berhasil Diupdate !');
				window.location.href='../jadwal_pelayanan';
				</script>";
			}else{
				echo "<script>
				alert('File yang anda upload bukan gambar atau lebih dari 5 Mb!');
				window.location.href='../jadwal_pelayanan/edit/$id';
				</script>";
			}
		}else{
			echo "<script>
			alert('Tidak ada perubahan data !');
			window.location.href='../jadwal_pelayanan';
			</script>";
		}
	}

}
