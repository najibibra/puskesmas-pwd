<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Kontak_Model->Get('tb_kontak');
		$data = array('data' => $data);
		$this->load->view('admin/kontak_list', $data);
	}

	public function tambah(){
		return $this->load->view('admin/kontak_tambah');
	}

	private function upload(){
		date_default_timezone_set('Asia/Jakarta');
		$config['upload_path'] = './assets/general';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = 1024 * 2; // <= 2Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($this->upload->do_upload("icon")){
			return $this->upload->data("file_name");
		} 
		else {
			return $this->upload->display_errors();
		}
	}

	public function aksi_tambah(){
		$jenis_sosmed = $this->input->post('jenis_sosmed');
		$nama_sosmed = $this->input->post('nama_sosmed');
		$url = $this->input->post('url');

		if ($jenis_sosmed == 'Lain-lain') {
			$judul_sosmed = $this->input->post('judul_sosmed');
			$jenis_sosmed = $judul_sosmed;
			$icon = $this->upload();
			if (strpos($this->upload->display_errors(), 'large')) {
				echo "<script>
				alert('Gambar yang anda upload lebih dari 2 Mb!');
				window.location.href='../kontak/tambah';
				</script>";
			} else if(strpos($this->upload->display_errors(), 'filetype')) {
				echo "<script>
				alert('File yang anda upload bukan gambar!');
				window.location.href='../kontak/tambah';
				</script>";
			} else {
				$data = array(
					'jenis_sosmed' => $jenis_sosmed,
					'nama_sosmed' => $nama_sosmed,
					'url' => $url,
					'icon' => $icon,
				);
				$this->Kontak_Model->insert($data);
				echo "<script>
				alert('Sosmed Berhasil Ditambahkan !');
				window.location.href='../kontak';
				</script>";
			}
		} else {
			if($jenis_sosmed = 'Facebook') {
				$icon = 'fab fa-facebook-square';
			} else if($jenis_sosmed = 'Instagram') {
				$icon = 'fab fa-instagram-square';
			} else if($jenis_sosmed = 'Twitter') {
				$icon = 'fab fa-twitter-square';
			} else if($jenis_sosmed = 'WhatsApp') {
				$icon = 'fab fa-whatsapp-square';
				$url = 'https://wa.me/082324240424' + $nama_sosmed;
			} else if($jenis_sosmed = 'Email') {
				$icon = 'fas fa-envelope-square';
				$url = 'mailto:' + $nama_sosmed;
			} else if($jenis_sosmed = 'Telepon') {
				$icon = 'fas fa-phone-square';
				$url = 'tel:' + $nama_sosmed;
			}
			$data = array(
				'jenis_sosmed' => $jenis_sosmed,
				'nama_sosmed' => $nama_sosmed,
				'url' => $url,
				'icon' => $icon,
			);
			$this->Kontak_Model->insert($data);
			echo "<script>
			alert('Sosmed Berhasil Ditambahkan !');
			window.location.href='../kontak';
			</script>";
		}
		
	}

	public function edit($id){
		$kontak=($this->Kontak_Model->ambil_data_id($id));
		$data = array(
            'id'   => set_value('id',$kontak->id),
            'jenis_sosmed'       => set_value('jenis_sosmed',$kontak->jenis_sosmed),
            'nama_sosmed'       => set_value('nama_sosmed',$kontak->nama_sosmed),
            'icon'       => set_value('icon',$kontak->icon),
            'url'       => set_value('url',$kontak->url),
            'action' 	  => site_url('admin/kontak/updateData')
		);
		$this->load->view('admin/kontak_edit', $data);
	}

	public function updateData(){
		$id = $this->input->post('id');
		$jenis_sosmed = $this->input->post('jenis_sosmed');
		$nama_sosmed = $this->input->post('nama_sosmed');
		$url = $this->input->post('url');

		if ($jenis_sosmed == 'Lain-lain') {
			$judul_sosmed = $this->input->post('judul_sosmed');
			$jenis_sosmed = $judul_sosmed;
			$path = './assets/general/';
			$case = array('id' => $id);
	
			$config['upload_path'] = './assets/general';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size'] = 1024 * 2; // <= 2Mb;
			$config['file_name'] = uniqid(date('dmY') . '_');
	
			$this->upload->initialize($config);
	
			if(!empty($_FILES['icon']['name'])){
				if($this->upload->do_upload('icon')){
					$file = $this->upload->data();
					$data = array(
						'jenis_sosmed' => $jenis_sosmed,
						'nama_sosmed' => $nama_sosmed,
						'url' => $url,
						'icon' => $file['file_name'],
					);
	
					@unlink($path.$this->input->post('file-lama'));
					$this->Kontak_Model->updateKontak($data, $case);
					echo "<script>
					alert('Sosmed Berhasil Diupdate !');
					window.location.href='../kontak';
					</script>";
				}else{
					echo "<script>
					alert('File yang anda upload bukan gambar atau lebih dari 2 Mb!');
					window.location.href='../kontak/edit/$id';
					</script>";
				}
			}else{
				$data = array(
					'jenis_sosmed' => $jenis_sosmed,
					'nama_sosmed' => $nama_sosmed,
					'url' => $url,
				);
				$this->Kontak_Model->updateKontak($data, $case);
				echo "<script>
				alert('Sosmed Berhasil Diupdate !');
				window.location.href='../kontak';
				</script>";
			}

		} else {
			if($jenis_sosmed === 'Facebook') {
				$icon = 'fab fa-facebook-square';
			} else if($jenis_sosmed === 'Instagram') {
				$icon = 'fab fa-instagram-square';
			} else if($jenis_sosmed === 'Twitter') {
				$icon = 'fab fa-twitter-square';
			} else if($jenis_sosmed = 'WhatsApp') {
				$icon = 'fab fa-whatsapp-square';
				$url = 'https://wa.me/082324240424' + $nama_sosmed;
			} else if($jenis_sosmed = 'Email') {
				$icon = 'fas fa-envelope-square';
				$url = 'mailto:' + $nama_sosmed;
			} else if($jenis_sosmed = 'Telepon') {
				$icon = 'fas fa-phone-square';
				$url = 'tel:' + $nama_sosmed;
			}
			$case = array('id' => $id);
			$data = array(
				'jenis_sosmed' => $jenis_sosmed,
				'nama_sosmed' => $nama_sosmed,
				'url' => $url,
				'icon' => $icon,
			);
			$this->Kontak_Model->updateKontak($data, $case);
			echo "<script>
			alert('Sosmed Berhasil Diupdate !');
			window.location.href='../kontak';
			</script>";
		}
	}

	public function hapus(){
		if ($_GET['jenis_sosmed'] != 'Facebook' && $_GET['jenis_sosmed'] != 'Instagram' && $_GET['jenis_sosmed'] != 'Twitter' && $_GET['jenis_sosmed'] != 'WhatsApp' && $_GET['jenis_sosmed'] != 'Email' && $_GET['jenis_sosmed'] != 'Telepon') {
			$path = 'assets/general/';
			@unlink($path.$_GET['icon']);
		}

		$where = array('id' => $_GET['id']);
		$this->Kontak_Model->delete($where);
		echo "<script>
		alert('Sosmed Berhasil Dihapus !');
		window.location.href='../kontak';
		</script>";
	}

}
