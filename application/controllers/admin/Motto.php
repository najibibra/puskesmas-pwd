<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motto extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Profil_Model->Get('tb_profil');
		$data = array('data' => $data);
		$this->load->view('admin/motto', $data);
	}

	public function edit($id){
		$motto=($this->Profil_Model->ambil_data_id($id));
		$data = array(
            'id'   => set_value('id',$motto->id),
            'motto'       => set_value('motto',$motto->motto),
            'action' 	  => site_url('admin/motto/updateData')
		);
		$this->load->view('admin/motto_edit', $data);
	}

	public function updateData(){
		date_default_timezone_set('Asia/Jakarta');
		$tgl_motto = date('Y:m:d H:i:s');
		$id = $this->input->post('id');
		$motto = $this->input->post('motto');
		$case = array('id' => $id);
		$data = array(
			'motto' => $motto,
			'tgl_motto' => $tgl_motto,
		);
		$this->Profil_Model->updateProfil($data, $case);
		echo "<script>
		alert('Motto Berhasil Diupdate !');
		window.location.href='../motto';
		</script>";
	}

}
