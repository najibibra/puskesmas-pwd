<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaduan extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Pengaduan_Model->Get('tb_pengaduan');
		$data = array('data' => $data);
		$this->load->view('admin/pengaduan_list', $data);
	}

	public function detail($id){
		$pengaduan=($this->Pengaduan_Model->ambil_data_id($id));
		$data = array(
		'id_pengaduan'   => set_value('id_pengaduan',$pengaduan->id_pengaduan),
		'nama_ciri' => set_value('nama_ciri',$pengaduan->nama_ciri),
		'ruangan'       => set_value('ruangan',$pengaduan->ruangan),
		'kritik_saran'       => set_value('kritik_saran',$pengaduan->kritik_saran),
		'tanggal'   => set_value('tanggal',$pengaduan->tanggal),
		);
		$this->load->view('admin/pengaduan_detail', $data);
	}

}
