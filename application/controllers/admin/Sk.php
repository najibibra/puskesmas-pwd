<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sk extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Sk_Model->Get('tb_sk');
		$data = array('data' => $data);
		$this->load->view('admin/sk_list', $data);
	}

	public function tambah(){
		return $this->load->view('admin/sk_tambah');
	}

	private function upload(){
		date_default_timezone_set('Asia/Jakarta');
		$config['upload_path'] = './assets/sk';
		$config['allowed_types'] = 'pdf|PDF';
		$config['max_size'] = 1024 * 7; // <= 7Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($this->upload->do_upload("file")){
			return $this->upload->data("file_name");
		} 
		else {
			return $this->upload->display_errors();
		}
	}

	public function aksi_tambah(){
		date_default_timezone_set('Asia/Jakarta');
		$judul = $this->input->post('judul');
		$tanggal = date('Y:m:d H:i:s');
		$file = $this->upload();
		if (strpos($this->upload->display_errors(), 'large')) {
			echo "<script>
			alert('Gambar yang anda upload lebih dari 7 Mb!');
			window.location.href='../sk/tambah';
			</script>";
		} else if(strpos($this->upload->display_errors(), 'filetype')) {
			echo "<script>
			alert('File yang anda upload bukan pdf!');
			window.location.href='../sk/tambah';
			</script>";
		} else {
			$data = array(
				'judul' => $judul,
				'file' => $file,
				'tanggal' => $tanggal,
			);
			$this->Sk_Model->insert($data);
			echo "<script>
			alert('SK Berhasil Ditambahkan !');
			window.location.href='../sk';
			</script>";
		}
	}

	public function edit($id){
		$sk=($this->Sk_Model->ambil_data_id($id));
		$data = array(
		'id_sk'   => set_value('id_sk',$sk->id_sk),
		'judul' => set_value('judul',$sk->judul),
		'file'   => set_value('file',$sk->file),
		'action' 	  => site_url('admin/sk/updateData')
		);
		$this->load->view('admin/sk_edit', $data);
	}

	public function updateData(){
		date_default_timezone_set('Asia/Jakarta');
		$id = $this->input->post('id_sk');
		$judul = $this->input->post('judul');
		$tanggal = date('Y:m:d H:i:s');
		$path = './assets/sk/';
		$case = array('id_sk' => $id);

		$config['upload_path'] = './assets/sk';
		$config['allowed_types'] = 'pdf|PDF';
		$config['max_size'] = 1024 * 8; // <= 8Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->upload->initialize($config);

		if(!empty($_FILES['file']['name'])){
			if($this->upload->do_upload('file')){
				$file = $this->upload->data();
				$data = array(
					'judul' => $judul,
					'file' => $file['file_name'],
					'tanggal' => $tanggal,
				);

				@unlink($path.$this->input->post('file-lama'));
				$this->Sk_Model->updateSk($data, $case);
				echo "<script>
				alert('SK Berhasil Diupdate !');
				window.location.href='../sk';
				</script>";
			}else{
				echo "<script>
				alert('File yang anda upload bukan pdf atau lebih dari 7 Mb!');
				window.location.href='../sk/edit/$id';
				</script>";
			} 
		}else{
			$data = array(
				'judul' => $judul,
				'tanggal' => $tanggal
			);
			$this->Sk_Model->updateSk($data, $case);
			echo "<script>
			alert('SK Berhasil Diupdate !');
			window.location.href='../sk';
			</script>";
		}
	}

	public function hapus(){
		$path = 'assets/sk/';
		@unlink($path.$_GET['file']);

		$where = array('id_sk' => $_GET['id']);
		$this->Sk_Model->delete($where);
		echo "<script>
		alert('SK Berhasil Dihapus !');
		window.location.href='../sk';
		</script>";
	}

}
