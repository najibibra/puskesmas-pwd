<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sop extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Sop_Model->Get('tb_sop');
		$data = array('data' => $data);
		$this->load->view('admin/sop_list', $data);
	}

	public function tambah(){
		return $this->load->view('admin/sop_tambah');
	}

	private function upload(){
		date_default_timezone_set('Asia/Jakarta');
		$config['upload_path'] = './assets/sop';
		$config['allowed_types'] = 'pdf|PDF';
		$config['max_size'] = 1024 * 7; // <= 7Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($this->upload->do_upload("file")){
			return $this->upload->data("file_name");
		} 
		else {
			return $this->upload->display_errors();
		}
	}

	public function aksi_tambah(){
		date_default_timezone_set('Asia/Jakarta');
		$judul = $this->input->post('judul');
		$tanggal = date('Y:m:d H:i:s');
		$file = $this->upload();
		if (strpos($this->upload->display_errors(), 'large')) {
			echo "<script>
			alert('Gambar yang anda upload lebih dari 7 Mb!');
			window.location.href='../sop/tambah';
			</script>";
		} else if(strpos($this->upload->display_errors(), 'filetype')) {
			echo "<script>
			alert('File yang anda upload bukan pdf!');
			window.location.href='../sop/tambah';
			</script>";
		} else {
			$data = array(
				'judul' => $judul,
				'file' => $file,
				'tanggal' => $tanggal,
			);
			$this->Sop_Model->insert($data);
			echo "<script>
			alert('SOP Berhasil Ditambahkan !');
			window.location.href='../sop';
			</script>";
		}
	}

	public function edit($id){
		$sop=($this->Sop_Model->ambil_data_id($id));
		$data = array(
		'id_sop'   => set_value('id_sop',$sop->id_sop),
		'judul' => set_value('judul',$sop->judul),
		'file'   => set_value('file',$sop->file),
		'action' 	  => site_url('admin/sop/updateData')
		);
		$this->load->view('admin/sop_edit', $data);
	}

	public function updateData(){
		date_default_timezone_set('Asia/Jakarta');
		$id = $this->input->post('id_sop');
		$judul = $this->input->post('judul');
		$tanggal = date('Y:m:d H:i:s');
		$path = './assets/sop/';
		$case = array('id_sop' => $id);

		$config['upload_path'] = './assets/sop';
		$config['allowed_types'] = 'pdf|PDF';
		$config['max_size'] = 1024 * 8; // <= 8Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->upload->initialize($config);

		if(!empty($_FILES['file']['name'])){
			if($this->upload->do_upload('file')){
				$file = $this->upload->data();
				$data = array(
					'judul' => $judul,
					'file' => $file['file_name'],
					'tanggal' => $tanggal,
				);

				@unlink($path.$this->input->post('file-lama'));
				$this->Sop_Model->updateSop($data, $case);
				echo "<script>
				alert('SOP Berhasil Diupdate !');
				window.location.href='../sop';
				</script>";
			}else{
				echo "<script>
				alert('File yang anda upload bukan pdf atau lebih dari 7 Mb!');
				window.location.href='../sop/edit/$id';
				</script>";
			} 
		}else{
			$data = array(
				'judul' => $judul,
				'tanggal' => $tanggal
			);
			$this->Sop_Model->updateSop($data, $case);
			echo "<script>
			alert('SOP Berhasil Diupdate !');
			window.location.href='../sop';
			</script>";
		}
	}

	public function hapus(){
		$path = 'assets/sop/';
		@unlink($path.$_GET['file']);

		$where = array('id_sop' => $_GET['id']);
		$this->Sop_Model->delete($where);
		echo "<script>
		alert('SOP Berhasil Dihapus !');
		window.location.href='../sop';
		</script>";
	}

}
