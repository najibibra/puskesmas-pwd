<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tatanilai extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Profil_Model->Get('tb_profil');
		$data = array('data' => $data);
		$this->load->view('admin/tatanilai', $data);
	}

	public function edit($id){
		$tatanilai=($this->Profil_Model->ambil_data_id($id));
		$data = array(
            'id'   => set_value('id',$tatanilai->id),
            'judul_tatanilai'       => set_value('judul_tatanilai',$tatanilai->judul_tatanilai),
            'isi_tatanilai'       => set_value('isi_tatanilai',$tatanilai->isi_tatanilai),
            'action' 	  => site_url('admin/tatanilai/updateData')
		);
		$this->load->view('admin/tatanilai_edit', $data);
	}

	public function updateData(){
		date_default_timezone_set('Asia/Jakarta');
		$tgl_tatanilai = date('Y:m:d H:i:s');
		$id = $this->input->post('id');
		$judul_tatanilai = $this->input->post('judul_tatanilai');
		$isi_tatanilai = $this->input->post('isi_tatanilai');
		$case = array('id' => $id);
		$data = array(
			'judul_tatanilai' => $judul_tatanilai,
			'isi_tatanilai' => addslashes($isi_tatanilai),
			'tgl_tatanilai' => $tgl_tatanilai,
		);
		$this->Profil_Model->updateProfil($data, $case);
		echo "<script>
		alert('Tata Nilai Berhasil Diupdate !');
		window.location.href='../tatanilai';
		</script>";
	}

}
