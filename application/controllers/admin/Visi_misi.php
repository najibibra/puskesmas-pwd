<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visi_misi extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Profil_Model->Get('tb_profil');
		$data = array('data' => $data);
		$this->load->view('admin/visi_misi', $data);
	}

	public function edit($id){
		$visi_misi=($this->Profil_Model->ambil_data_id($id));
		$data = array(
            'id'   => set_value('id',$visi_misi->id),
            'visi'       => set_value('visi',$visi_misi->visi),
            'misi'       => set_value('misi',$visi_misi->misi),
            'action' 	  => site_url('admin/visi_misi/updateData')
		);
		$this->load->view('admin/visi_misi_edit', $data);
	}

	public function updateData(){
		date_default_timezone_set('Asia/Jakarta');
		$tgl_visimisi = date('Y:m:d H:i:s');
		$id = $this->input->post('id');
		$visi = $this->input->post('visi');
		$misi = $this->input->post('misi');
		$case = array('id' => $id);
		$data = array(
			'visi' => $visi,
			'misi' => addslashes($misi),
			'tgl_visimisi' => $tgl_visimisi,
		);
		$this->Profil_Model->updateProfil($data, $case);
		echo "<script>
		alert('Visi Misi Berhasil Diupdate !');
		window.location.href='../visi_misi';
		</script>";
	}

}
