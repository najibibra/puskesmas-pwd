<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri_Model extends CI_Model {

  public $table = 'tb_galeri';
  public $id = 'id_galeri';

  public function Get($table){
    $this->db->order_by('id_galeri',"DESC");
    $res = $this->db->get($table);
    return $res->result_array();
  }

  public function GetFE($table){
    $this->db->where_not_in('tayang',0);
    $this->db->order_by('id_galeri',"DESC");
    $res = $this->db->get($table);
    return $res->result_array();
  }

  function ambil_data_id($id){
    $this->db->where($this->id,$id);
    return $this->db->get($this->table)->row();
  }

  public function insert($data){
    $this->db->insert('tb_galeri', $data);
    return TRUE;
  }

  public function simpan_galeri($keterangan, $tanggal, $file){
    $data = array(
        'keterangan' => $keterangan,
        'tanggal' => $tanggal,
        'gambar' => $file
    );
    $result = $this->db->insert('tb_galeri', $data);
    return $result;
  }

  public function updateGaleri($data, $case){
    $this->db->update($this->table, $data, $case);
    return TRUE;
  }

  private function deleteImage($id){
    $galeri = $this->ambil_data_id($id);
    $filename = explode(".", $galeri->file)[0];
    return array_map('unlink', glob(FCPATH."./assets/galeri/$filename.*"));
  }

  public function delete($where){
    $this->db->where($where);
    $this->db->delete($this->table);
    return TRUE;
  }

}
?>
