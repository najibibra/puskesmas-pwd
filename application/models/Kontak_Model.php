<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak_Model extends CI_Model {

  public $table = 'tb_kontak';
  public $id = 'id';

  public function Get($table)
  {
    $res = $this->db->get($table);
    return $res->result_array();
  }

  public function insert($data){
    $this->db->insert('tb_kontak', $data);
    return TRUE;
  }

  function ambil_data_id($id)
  {
    $this->db->where($this->id,$id);
    return $this->db->get($this->table)->row();
  }

  function edit_data($id, $data)
  {
    $this->db->where($this->id, $id);
    $this->db->update($this->table,$data);
  }

  public function tampilUbahKontak($id){
    return $this->db->select('*')
                    ->where('id', $id)
                    ->get($this->table);
  }

  public function updateKontak($data, $case){
    $this->db->update($this->table, $data, $case);
    return TRUE;
  }

  public function delete($where){
    $this->db->where($where);
    $this->db->delete($this->table);
    return TRUE;
  }

}
?>
