<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelayanan_Model extends CI_Model {

  public $table = 'tb_pelayanan';
  public $id = 'id';

  public function Get($table)
  {
    $res = $this->db->get($table);
    return $res->result_array();
  }

  function ambil_data_id($id)
  {
    $this->db->where($this->id,$id);
    return $this->db->get($this->table)->row();
  }

  function edit_data($id, $data)
  {
    $this->db->where($this->id, $id);
    $this->db->update($this->table,$data);
  }

  public function tampilUbahPelayanan($id){
    return $this->db->select('*')
                    ->where('id', $id)
                    ->get($this->table);
  }

  public function updatePelayanan($data, $case){
    $this->db->update($this->table, $data, $case);
    return TRUE;
  }

}
?>
