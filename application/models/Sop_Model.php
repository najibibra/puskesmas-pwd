<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sop_Model extends CI_Model {

  public $table = 'tb_sop';
  public $id = 'id_sop';

  public function Get($table){
    $res = $this->db->get($table);
    return $res->result_array();
  }

  function ambil_data_id($id){
    $this->db->where($this->id,$id);
    return $this->db->get($this->table)->row();
  }

  public function insert($data){
    $this->db->insert('tb_sop', $data);
    return TRUE;
  }

  public function simpan_sop($judul, $isi, $file){
    $data = array(
        'judul' => $judul,
        'isi' => $isi,
        'gambar' => $file
    );
    $result = $this->db->insert('tb_sop', $data);
    return $result;
  }

  function edit_data($id, $data){
    $this->db->where($this->id, $id);
    $this->db->update($this->table,$data);
  }

  private function deleteImage($id){
    $sop = $this->ambil_data_id($id);
    $filename = explode(".", $sop->file)[0];
    return array_map('unlink', glob(FCPATH."./assets/sop/$filename.*"));
  }

  public function tampilUbahSop($id){
    return $this->db->select('*')
                    ->where('id_sop', $id)
                    ->get($this->table);
  }

  public function updateSop($data, $case){
    $this->db->update($this->table, $data, $case);
    return TRUE;
  }

  public function delete($where){
    $this->db->where($where);
    $this->db->delete($this->table);
    return TRUE;
  }

}
?>
