<?php
$this->load->view('admin/header');
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Hubungi Kami</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <form action="<?php echo base_url()?>admin/kontak/updateData" method="post" enctype="multipart/form-data">
              <input type="hidden" name="id" value="<?= $id; ?>">
              <input type="hidden" name="file-lama" value="<?= $icon; ?>">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-3">
                    <div class="form-group">
                        <label>Jenis Sosmed</label>
                        <select class="form-control" name="jenis_sosmed" id="jenis_sosmed" onchange="jenisSosmed()" required>
                          <option value="Facebook" <?= $jenis_sosmed === 'Facebook' ? 'selected' : '';?>>Facebook</option>
                          <option value="Instagram" <?= $jenis_sosmed === 'Instagram' ? 'selected' : '';?>>Instagram</option>
                          <option value="Twitter" <?= $jenis_sosmed === 'Twitter' ? 'selected' : '';?>>Twitter</option>
                          <option value="WhatsApp" <?= $jenis_sosmed ==='WhatsApp' ? 'selected': '';?>>WhatsApp</option>
                          <option value="Email" <?= $jenis_sosmed === 'Email' ? 'selected' : '';?>>Email</option>
                          <option value="Telepon" <?= $jenis_sosmed === 'Telepon' ? 'selected' : '';?>>Telepon</option>
                          <option value="Lain-lain" <?= $jenis_sosmed !== 'Facebook' && $jenis_sosmed !== 'Instagram' && $jenis_sosmed !== 'Twitter' && $jenis_sosmed !== 'WhatsApp' && $jenis_sosmed !== 'Email' && $jenis_sosmed !== 'Telepon' ? 'selected' : '';?>>Lain-lain</option>
                        </select>
                    </div>
                  </div>
                  <div class="col-sm-9">
                    <div class="form-group">
                    <label>ID / No Sosmed</label>
                    <input type="text" class="form-control" placeholder="Nama Sosmed" value="<?= $nama_sosmed;?>" name="nama_sosmed" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div id="judulSosmed" class="col-sm-3" style="display: none;">
                    <div class="form-group">
                        <label>Judul Sosmed</label>
                        <input type="text" class="form-control" placeholder="Judul Sosmed" name="judul_sosmed" id="judul_sosmed" value="<?= $jenis_sosmed;?>">
                    </div>
                  </div>
                  <div id="uploadSosmed" class="col-sm-9" style="display: none;">
                    <div class="form-group">
                      <label>Gambar</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" name="icon" id="icon" value="<?= $icon;?>">
                          <label class="custom-file-label"><?= $icon;?></label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="url" style="display: none;" class="form-group">
                    <label>Link Sosmed</label>
                    <input type="text" class="form-control" placeholder="Link Sosmed" value="<?= $url;?>" name="url" required>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="simpan">SIMPAN</button>
              </div>
            </form>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>

<script>
  $( document ).ready(function() {
    const value = document.getElementById("jenis_sosmed").value;
    const judulSosmed = document.getElementById("judulSosmed");
    const uploadSosmed = document.getElementById("uploadSosmed");
    const url = document.getElementById("url");
    if (value === 'Lain-lain') {
      judulSosmed.style.display = "block";
      uploadSosmed.style.display = "block";
      url.style.display = "block";
      judulSosmed.setAttribute('required','required');
      uploadSosmed.setAttribute('required','required');
      url.setAttribute('required','required');
    } else {
      if (value === 'Facebook' || value === 'Instagram' || value === 'Twitter') {
        url.style.display = "block";
        url.setAttribute('required','required');
      } else {
        url.style.display = "none";
        url.removeAttribute('required'); 
      }
      judulSosmed.style.display = "none";
      uploadSosmed.style.display = "none";
      judulSosmed.removeAttribute('required'); 
      uploadSosmed.removeAttribute('required'); 
    }
  });
</script>