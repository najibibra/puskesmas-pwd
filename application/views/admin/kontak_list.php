<?php
  $this->load->view('admin/header');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Hubungi Kami</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-body">
                <a href="<?php echo base_url(); ?>admin/kontak/tambah" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Posting Artikel"><i class="fa fa-plus"></i> TAMBAH DATA</a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th scope="col" width="25px">No</th>
                    <th>Jenis Sosmed</th>
                    <th>ID / No Sosmed</th>
                    <th>Url Sosmed</th>
                    <th>Gambar</th>
                    <th style="min-width: 92px">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $no=1; foreach ($data as $kontak): ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $kontak['jenis_sosmed']; ?></td>
                        <td><?= $kontak['nama_sosmed']; ?></td>
                        <td>
                          <a href="<?= $kontak['url']; ?>" target="_blank">
                            <?= $kontak['url']; ?>
                          </a>  
                        </td>
                        <td>
                        <?php if ($kontak['jenis_sosmed'] != 'Facebook' && $kontak['jenis_sosmed'] != 'Instagram' && $kontak['jenis_sosmed'] != 'Twitter' && $kontak['jenis_sosmed'] != 'WhatsApp' && $kontak['jenis_sosmed'] != 'Email' && $kontak['jenis_sosmed'] != 'Telepon') { ?>
                          <a href="../assets/general/<?= $kontak['icon']; ?>" target="_blank">
                            <img src="../assets/general/<?= $kontak['icon']; ?>" style="max-width: 65px;max-height: 65px;">
                          </a>
                        <?php } else { ?>
                          default
                        <?php } ?>
                        </td>
                        <td>
                          <a href="<?= site_url('admin/kontak/edit/'.$kontak['id']) ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit Kontak">EDIT</a>
                          <a href="<?= site_url('admin/kontak/hapus?id='.$kontak['id'].'&jenis_sosmed='.$kontak['jenis_sosmed'].'&icon='.$kontak['icon']) ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus Kontak">HAPUS</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>