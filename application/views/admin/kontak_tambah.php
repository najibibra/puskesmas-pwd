<?php
$this->load->view('admin/header');
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah Hubungi Kami</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <form action="<?php echo base_url()?>admin/kontak/aksi_tambah" method="post" enctype="multipart/form-data">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-3">
                    <div class="form-group">
                        <label>Jenis Sosmed</label>
                        <select id="jenis_sosmed" onchange="jenisSosmed()" class="form-control" name="jenis_sosmed" required>
                          <option value="">- Pilih Jenis Sosmed -</option>
                          <option value="Facebook">Facebook</option>
                          <option value="Instagram">Instagram</option>
                          <option value="Twitter">Twitter</option>
                          <option value="WhatsApp">WhatsApp</option>
                          <option value="Email">Email</option>
                          <option value="Telepon">Telepon</option>
                          <option value="Lain-lain">Lain-lain</option>
                        </select>
                    </div>
                  </div>
                  <div class="col-sm-9">
                    <div class="form-group">
                    <label>ID / No Sosmed</label>
                    <input type="text" class="form-control" placeholder="Nama Sosmed" name="nama_sosmed" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div id="judulSosmed" class="col-sm-3" style="display: none;">
                    <div class="form-group">
                        <label>Judul Sosmed</label>
                        <input type="text" class="form-control" placeholder="Judul Sosmed" name="judul_sosmed" id="judul_sosmed">
                    </div>
                  </div>
                  <div id="uploadSosmed" class="col-sm-9" style="display: none;">
                    <div class="form-group">
                      <label>Gambar</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" name="icon" id="icon">
                          <label class="custom-file-label">Upload Gambar max 2 Mb</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="url" style="display: none;" class="form-group">
                    <label>Link Sosmed</label>
                    <input type="text" class="form-control" placeholder="Link Sosmed" name="url" required>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="simpan">SIMPAN</button>
              </div>
            </form>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>