<?php
  $this->load->view('admin/header');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Surat Keputusan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-body">
                <a href="<?php echo base_url(); ?>admin/sk/tambah" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Upload SK"><i class="fa fa-plus"></i> UPLOAD SK</a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th scope="col" width="25px">No</th>
                    <th>Judul SK</th>
                    <th>File</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $no=1; foreach ($data as $sk): ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $sk['judul']; ?></td>
                        <td>
                          <a href="../assets/sk/<?= $sk['file']; ?>" target="_blank">
                            <?= $sk['file']; ?>
                          </a>
                        </td>
                        <td>
                          <a href="<?= site_url('admin/sk/edit/'.$sk['id_sk']) ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit SK">EDIT</a>
                          <a href="<?= site_url('admin/sk/hapus?id='.$sk['id_sk'].'&file='.$sk['file']) ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus SK">HAPUS</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>