<?php
  $this->load->view('admin/header');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data SOP</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-body">
                <a href="<?php echo base_url(); ?>admin/sop/tambah" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Upload SOP"><i class="fa fa-plus"></i> UPLOAD SOP</a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th scope="col" width="25px">No</th>
                    <th>Judul SOP</th>
                    <th>File</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $no=1; foreach ($data as $sop): ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $sop['judul']; ?></td>
                        <td>
                          <a href="../assets/sop/<?= $sop['file']; ?>" target="_blank">
                            <?= $sop['file']; ?>
                          </a>
                        </td>
                        <td>
                          <a href="<?= site_url('admin/sop/edit/'.$sop['id_sop']) ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit SOP">EDIT</a>
                          <a href="<?= site_url('admin/sop/hapus?id='.$sop['id_sop'].'&file='.$sop['file']) ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus SOP">HAPUS</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>