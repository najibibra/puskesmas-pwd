<?php
  $this->load->view('admin/header');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Visi Misi</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <a href="<?php echo site_url('admin/visi_misi/edit/1') ?>" class="btn btn-primary float-sm-right" >EDIT</a>
              </div>
              <div class="card-body">
                <p><b>Visi :</b></p>
                <?php foreach ($data as $visi_misi): ?><?= htmlspecialchars_decode(stripcslashes($visi_misi['visi'])); ?><?php endforeach; ?>
                <br></br>
                <p><b>Misi :</b></p>
                <?php foreach ($data as $visi_misi): ?><?= htmlspecialchars_decode(stripcslashes($visi_misi['misi'])); ?><?php endforeach; ?>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>