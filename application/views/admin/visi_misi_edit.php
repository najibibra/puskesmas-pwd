<?php
$this->load->view('admin/header');
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Visi Misi</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <form action="<?php echo base_url()?>admin/visi_misi/updateData" method="post">
              <input type="hidden" name="id" value="<?= $id; ?>">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label>Visi</label>
                      <input type="text" class="form-control" placeholder="Visi" value="<?= $visi;?>" name="visi" required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Misi</label>
                  <textarea id="summernote" class="form-control" style="height: 250px" name="misi" required>
                    <?= $misi;?>
                  </textarea>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="simpan">SIMPAN</button>
              </div>
            </form>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>