<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <!-- Start Hubungi Kami Section -->
  <section>
    <br>
    <div class="container">
      <div class="st-section-heading st-style1">
        <br>
        <div class="row">
          <?php foreach ($dataKontak as $kontak): ?>
            <div class="col-lg-4" style="margin-bottom: 30px;">
              <a href="<?= $kontak['url'];?>" target="_blank">
                <div class="st-iconbox st-style1">
                <?php if ($kontak['jenis_sosmed'] != 'Facebook' && $kontak['jenis_sosmed'] != 'Instagram' && $kontak['jenis_sosmed'] != 'Twitter' && $kontak['jenis_sosmed'] != 'WhatsApp' && $kontak['jenis_sosmed'] != 'Email' && $kontak['jenis_sosmed'] != 'Telepon') { ?>
                  <div class="st-iconbox-icon st-blue-box">
                    <img src="assets/general/<?= $kontak['icon']; ?>" style="max-width: 46px;max-height: 46px;">
                  </div>
                <?php } else { ?>
                  <div class="st-iconbox-icon st-blue-box">
                    <i class="<?= $kontak['icon'];?>"></i>
                  </div>
                <?php } ?>
                  <h2 class="st-iconbox-title"><?= $kontak['jenis_sosmed'];?></h2>
                  <div class="st-iconbox-text"><?= $kontak['nama_sosmed'];?></div>
                </div>
              </a>
              <div class="st-height-b0 st-height-lg-b30"></div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
    <br><br>
  </section>
  <!-- End Hubungi Kami Section -->
  
  <div class="st-google-map">
    <iframe
      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3959.248348229386!2d110.90998931459386!3d-7.0971874948746425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e70b00d03b89659%3A0xda90c8467cc11ee9!2sPuskesmas%20Purwodadi%201!5e0!3m2!1sen!2sid!4v1660133995790!5m2!1sen!2sid"
      allowfullscreen></iframe>
  </div>

<?php
  $this->load->view('footer', $dataKontak);
?>