<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <div class="st-height-b50 st-height-lg-b80"></div>
  <!-- Start Motto Section -->
  <section id="motto">
    <div class="container">
      <div class="st-section-heading st-style1">
        <h2 class="st-section-heading-title">Motto</h2>
        <div class="st-seperator">
          <div class="st-seperator-left wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s"></div>
          <div class="st-seperator-center"><img src="assets/img/icon.png" alt="icon"></div>
          <div class="st-seperator-right wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s"></div>
        </div>
        <br>
        <div class="st-post-details st-style1">
          <div class="st-post-info">
            <div class="st-post-text wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="0.2s">
              <blockquote><i><?php foreach ($data as $motto): echo htmlspecialchars_decode(stripcslashes($motto['motto'])); endforeach; ?></i></blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br><br>
  </section>
  <!-- End Motto Section -->
  <div class="st-height-b50 st-height-lg-b80"></div>

<?php
  $this->load->view('footer', $dataKontak);
?>