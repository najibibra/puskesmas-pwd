<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <!-- Start Pengaduan Saran Section -->
  <section class="st-shape-wrap">
    <div class="st-shape1"><img src="<?php echo base_url(); ?>assets/img/shape/contact-shape1.svg" alt="shape1"></div>
    <div class="st-shape2"><img src="<?php echo base_url(); ?>assets/img/shape/contact-shape2.svg" alt="shape2"></div>
    <div class="st-height-b100 st-height-lg-b80"></div>
    <div class="container">
      <div class="st-section-heading st-style1">
        <h2 class="st-section-heading-title">Pengaduan Saran</h2>
        <div class="st-seperator">
          <div class="st-seperator-left wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s"></div>
          <div class="st-seperator-center"><img src="<?php echo base_url(); ?>assets/img/icon.png" alt="icon"></div>
          <div class="st-seperator-right wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s"></div>
        </div>
      </div>
      <br>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-10 offset-lg-1">
          <div id="st-alert"></div>
          <form action="<?php echo site_url('pengaduan/aksi_tambah') ?>" class="row st-contact-form st-type1" method="post" id="contact-form">
            <div class="col-lg-6">
              <div class="st-form-field st-style1">
                <label>Nama / Ciri Pegawai</label>
                <input type="text" id="nama_ciri" name="nama_ciri" placeholder="Nama / Ciri Pegawai" required>
              </div>
            </div><!-- .col -->
            <div class="col-lg-6">
              <div class="st-form-field st-style1">
                <label>Nama Ruangan</label>
                <input type="text" id="ruangan" name="ruangan" placeholder="Nama Ruangan" required>
              </div>
            </div><!-- .col -->
            <div class="col-lg-12">
              <div class="st-form-field st-style1">
                <label>Kritik & Saran</label>
                <textarea cols="30" rows="10" id="kritik_saran" name="kritik_saran" placeholder="Kritik & Saran..."
                  required></textarea>
              </div>
            </div><!-- .col -->
            <div class="col-lg-12">
              <div class="text-center">
                <div class="st-height-b10 st-height-lg-b10"></div>
                <button class="st-btn st-style1 st-color1 st-size-medium" type="submit" id="simpan" name="simpan">Kirim Pengaduan</button>
              </div>
            </div><!-- .col -->
          </form>
        </div><!-- .col -->
      </div>
    </div>
    <div class="st-height-b120 st-height-lg-b80"></div>
  </section>
  <!-- End Pengaduan Saran Section -->

<?php
    $this->load->view('footer', $dataKontak);
?>