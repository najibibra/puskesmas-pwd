<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <div class="container">
    <br>
    <div class="row">
      <div class="col-lg-12">
        <h3 style="border-bottom: rgb(17, 193, 178) 0.4px solid; padding-bottom: 12px">Standar Operasional Prosedur</h3>
        <table style="width: 100%;">
            <?php foreach ($data as $sop): ?>
            <tr>
                <td style="color: #0cb8b6">
                    <a href="assets/sop/<?= $sop['file']; ?>" target="_blank">
                        <?= $sop['judul']; ?>
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
      </div>
    </div>
    <br><br>
  </div>

  <div class="st-height-b50 st-height-lg-b80"></div>

<?php
    $this->load->view('footer', $dataKontak);
?>