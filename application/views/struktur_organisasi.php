<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <!-- Start struktur Section -->
  <section id="struktur">
    <br>
    <div class="container">
      <div class="st-section-heading st-style1">
        <h2 class="st-section-heading-title">Struktur Organisasi</h2>
        <div class="st-seperator">
          <div class="st-seperator-left wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s"></div>
          <div class="st-seperator-center"><img src="assets/img/icon.png" alt="icon"></div>
          <div class="st-seperator-right wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s"></div>
        </div>
        <br>
        <div class="st-post-details st-style1">
          <img class="st-zoom-in" src="assets/general/<?php foreach ($data as $struktur): echo $struktur['gambar']; endforeach; ?>" alt="blog1">
        </div>
      </div>
    </div>
  </section>
  <!-- End struktur Section -->
  <div class="st-height-b50 st-height-lg-b80"></div>

<?php
    $this->load->view('footer', $dataKontak);
?>