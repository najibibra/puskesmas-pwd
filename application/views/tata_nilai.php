<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <!-- Start Tata Nilai Section -->
  <section class="st-shape-wrap">
    <div class="st-shape1"><img src="assets/img/shape/contact-shape1.svg" alt="shape1"></div>
    <div class="st-shape2"><img src="assets/img/shape/contact-shape2.svg" alt="shape2"></div>
    <div class="st-height-b120 st-height-lg-b80"></div>
    <div class="container">
      <div class="st-section-heading st-style1">
        <h2 class="st-section-heading-title">Tata Nilai</h2>
        <div class="st-seperator">
          <div class="st-seperator-left wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s"></div>
          <div class="st-seperator-center"><img src="assets/img/icon.png" alt="icon"></div>
          <div class="st-seperator-right wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s"></div>
        </div>
        <br>
        <?php foreach ($data as $tatanilai): ?>
        <h3 class="st-iconbox-title"><?= $tatanilai['judul_tatanilai']; ?></h3>
        <div class="row" style="margin-left:20px !important;">
          <div class="col-md-1"></div>
          <div class="col-md-10">
            <div class="wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="0.2s" style="text-align: left">
              <?= htmlspecialchars_decode(stripcslashes($tatanilai['isi_tatanilai'])); ?>
            </div>
          </div>
        </div>
        <?php endforeach; ?>
      </div>
      <br>
    </div>
    <div class="st-height-b120 st-height-lg-b80"></div>
  </section>
  <!-- End Tata Nilai Section -->
  <div class="st-height-b50 st-height-lg-b80"></div>

<?php
    $this->load->view('footer', $dataKontak);
?>