<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <!-- Start Visi Misi Section -->
  <section id="visiMisi">
    <br><br>
    <div class="container">
      <div class="st-section-heading st-style1">
        <h2 class="st-section-heading-title">Visi & Misi</h2>
        <div class="st-seperator">
          <div class="st-seperator-left wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s"></div>
          <div class="st-seperator-center"><img src="assets/img/icon.png" alt="icon"></div>
          <div class="st-seperator-right wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s"></div>
        </div>
        <br>
        <?php foreach ($data as $visi_misi): ?>
        <h3 class="st-iconbox-title">Visi</h3>
        <div class="st-section-heading-subtitle wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="0.2s">
          <?= $visi_misi['visi']; ?>
        </div><br>
        <h3 class="st-iconbox-title">Misi</h3>
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-8">
            <div class="wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="0.2s" style="text-align: left">
              <div class="st-section-heading-subtitle">
                <?= htmlspecialchars_decode(stripcslashes($visi_misi['misi'])); ?>
              </div>
            </div>
          </div>
        </div>
        <?php endforeach; ?>
      </div>
    </div>
  </section>
  <!-- End Visi Misi Section -->
  <div class="st-height-b50 st-height-lg-b80"></div>

<?php
  $this->load->view('footer', $dataKontak);
?>